/***************************************************************************
*   Copyright (C) 2008 by Frans Schreuder                                 *
*   usbpicprog.sourceforge.net                                            *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

// NOTE: to avoid lots of warnings with MSVC 2008 about deprecated CRT functions
//       it's important to include wx/defs.h before STL headers
#include <wx/defs.h>

// IMPORTANT: first include wxWidgets headers (indirectly)
#include <wx/log.h>
#include "hardware_uc.h"
#include "uppmainwindow.h"

// NOW we can include the <usb.h> header without compiling problems
#include <libusb.h>
#include <iostream>

#ifdef __WXMSW__
#include <wx/msw/msvcrt.h> // useful to catch memory leaks when compiling under MSVC
#endif

using namespace std;
//#define USB_DEBUG 3

#ifndef __WXMSW__
const char *libusb_strerror(enum libusb_error errcode) {
    switch (errcode) {
    case LIBUSB_SUCCESS:
        return "Success";
    case LIBUSB_ERROR_IO:
        return "Input/output error";
    case LIBUSB_ERROR_INVALID_PARAM:
        return "Invalid parameter";
    case LIBUSB_ERROR_ACCESS:
        return "Access denied (insufficient permissions)";
    case LIBUSB_ERROR_NO_DEVICE:
        return "No such device (it may have been disconnected)";
    case LIBUSB_ERROR_NOT_FOUND:
        return "Entity not found";
    case LIBUSB_ERROR_BUSY:
        return "Resource busy";
    case LIBUSB_ERROR_TIMEOUT:
        return "Operation timed out";
    case LIBUSB_ERROR_OVERFLOW:
        return "Overflow";
    case LIBUSB_ERROR_PIPE:
        return "Pipe error";
    case LIBUSB_ERROR_INTERRUPTED:
        return "System call interrupted (perhaps due to signal)";
    case LIBUSB_ERROR_NO_MEM:
        return "Insufficient memory";
    case LIBUSB_ERROR_NOT_SUPPORTED:
        return "Operation not supported or unimplemented on this platform";
    case LIBUSB_ERROR_OTHER:
        return "Other error";

    default:
        return "Unknown error";
    }
}
#endif

// ----------------------------------------------------------------------------
// HardwareUC
// ----------------------------------------------------------------------------

HardwareUC::HardwareUC() {
#ifdef USB_DEBUG
    cout << "USB debug enabled, remove the '#define USB_DEBUG 3' line in "
            "hardware.cpp to disable it" << endl;
    libusb_set_debug(NULL, USB_DEBUG);
#endif

    // init all internal variables
    m_handle = NULL;
    m_modeReadEndpoint = m_modeWriteEndpoint = LIBUSB_TRANSFER_TYPE_INTERRUPT;
}

HardwareUC::~HardwareUC() {
    disconnect();
}

void HardwareUC::setConnParams(HardwareType hwtype) {
    m_hwtype = hwtype;
}

bool HardwareUC::connect(UppMainWindow *CB) {
    int retcode;

    m_abortOperations = false;
    m_handle = NULL;
    m_pCallBack = CB;

    if (m_hwtype == HW_UPP) {
        // search first the UPP and then, if no UPP is found, the bootloader

        m_handle =
            libusb_open_device_with_vid_pid(NULL, UPP_VENDOR, UPP_PRODUCT);
        if (m_handle) {
            // found the UPP programmer
            m_hwCurrent = HW_UPP;
        }
        /*else
        {
                    // failed to open this device, choose the next one
            m_handle = libusb_open_device_with_vid_pid(NULL, BOOTLOADER_VENDOR,
        BOOTLOADER_PRODUCT);
            if (m_handle)
                m_hwCurrent=HW_BOOTLOADER;       // found the UPP bootloader
        }*/
    } else {
        // search first the bootloader and then, if no bootloader is found, the
        // UPP

        m_handle = libusb_open_device_with_vid_pid(NULL, BOOTLOADER_VENDOR,
                                                   BOOTLOADER_PRODUCT);
        /*if ( (retcode=libusb_reset_device(m_handle)) != LIBUSB_SUCCESS )
        {
                wxLogError(_("Couldn't reset the USB device interface: %s"),
        libusb_strerror((libusb_error)retcode));
                m_hwCurrent = HW_NONE;
                m_handle = NULL;
                return false;
        }*/
        if (m_handle) {
            // found the UPP bootloader
            m_hwCurrent = HW_BOOTLOADER;
        }
        /*else
        {
                        disconnect();`
            // failed to open this device, choose the next one
            m_handle = libusb_open_device_with_vid_pid(NULL, UPP_VENDOR,
        UPP_PRODUCT);
            if (m_handle)
                m_hwCurrent=HW_UPP;       // found the UPP
        }*/
    }

    if (m_handle == NULL) {
        m_hwCurrent = HW_NONE;
        m_protocol = PROT_NONE;
        return false;
    }

// we've found some hardware!

#if 0 // FIXME: why is this needed??
    if ( (retcode=libusb_reset_device(m_handle)) != LIBUSB_SUCCESS )
    {
        wxLogError(_("Couldn't reset the USB device interface: %s"), libusb_strerror((libusb_error)retcode));
        m_hwCurrent = HW_NONE;
        m_handle = NULL;
        return false;
    }

    libusb_close(m_handle);
    if (m_hwCurrent == HW_UPP)
        m_handle = libusb_open_device_with_vid_pid(NULL, UPP_VENDOR, UPP_PRODUCT);
    else
        m_handle = libusb_open_device_with_vid_pid(NULL, BOOTLOADER_VENDOR, BOOTLOADER_PRODUCT);

    if (!m_handle)
    {
        m_hwCurrent = HW_NONE;
        return false;
    }
#endif

    // NOTE: the firmware in the UPP has a single configuration with index #1;
    //       see the CFG01.bCfgValue in uc_code/usbdsc.c
    if ((retcode = libusb_set_configuration(m_handle, 1)) != LIBUSB_SUCCESS) {
        wxLogError(_("Error setting configuration of the USB device: %s"),
                   libusb_strerror((libusb_error)retcode));
        m_hwCurrent = HW_NONE;
        m_protocol = PROT_NONE;
        m_handle = NULL;
        return false;
    }

    // claim the USB interface
    // NOTE: the firmware in the UPP has a single interface with index #0:
    //       see the CFG01.bInterfaceNumber in uc_code/usbdsc.c
    if ((retcode = libusb_claim_interface(m_handle, 0)) != LIBUSB_SUCCESS) {
        wxLogError(_("Error claiming the USB device interface: %s"),
                   libusb_strerror((libusb_error)retcode));
        m_hwCurrent = HW_NONE;
        m_protocol = PROT_NONE;
        m_handle = NULL;
        return false;
    }

// TODO: as libusb-1.0 docs in the "caveats" page say we should check here that
// the interface we claimed
//       still has the same configuration we set previously...
// 		 but anyway, usbpicprog will never switch

// now save the endpoint mode for both the READ_ENDPOINT and the WRITE_ENDPOINT
#if 0
    if ( (retcode=libusb_get_descriptor(m_handle, LIBUSB_DT_ENDPOINT, ENDPOINT_INDEX, (unsigned char*)&ed, sizeof(ed))) != LIBUSB_SUCCESS )
    {
        wxLogError(_("Error getting USB endpoint descriptor: %s"), libusb_strerror((libusb_error)retcode));
        m_hwCurrent = HW_NONE;
        m_handle = NULL;
        return false;
    }

    m_modeReadEndpoint = ed.bmAttributes & LIBUSB_TRANSFER_TYPE_MASK;

    if ( (retcode=libusb_get_descriptor(m_handle, LIBUSB_DT_ENDPOINT, WRITE_ENDPOINT, (unsigned char*)&ed, sizeof(ed))) != LIBUSB_SUCCESS )
    {
        wxLogError(_("Error getting USB endpoint descriptor: %s"), libusb_strerror((libusb_error)retcode));
        m_hwCurrent = HW_NONE;
        m_handle = NULL;
        return false;
    }

    m_modeWriteEndpoint = ed.bmAttributes & LIBUSB_TRANSFER_TYPE_MASK;
#else
    // IMPORTANT: currently the firmware does not support retrieving the
    // endpoint descriptors so we have to hardcode
    //            the type of the endpoints here:
    if (m_hwCurrent == HW_UPP)
        m_modeReadEndpoint = m_modeWriteEndpoint =
            LIBUSB_TRANSFER_TYPE_INTERRUPT;
    else
        m_modeReadEndpoint = m_modeWriteEndpoint = LIBUSB_TRANSFER_TYPE_BULK;
#endif

    // everything completed successfully:

    wxASSERT(m_handle != NULL && m_hwCurrent != HW_NONE);
    if (m_hwCurrent == HW_BOOTLOADER)
        m_protocol = PROT_BOOTLOADER0;
    else {
        unsigned char msg[64];

        msg[0] = CMD_GET_PROTOCOL_VERSION;
        writeString(msg, 1, true); // ignore errors (if we are disconnected then
                                   // we will figure it out next time)
        int nBytes = readString(
            msg, 1,
            true); // unfortunately this results in a timeout for PROT_UPP0
        if (nBytes > 0)
            m_protocol = (PROTOCOL)(PROT_UPP0 + msg[0]);
        else
            m_protocol = PROT_UPP0;
    }
    return true;
}

bool HardwareUC::disconnect() {
    bool success = true;

    if (m_handle) {
        // unsigned char msg[64];
        // msg[0]=CMD_BOOT_RESET;
        // if(m_hwCurrent==HW_BOOTLOADER)writeString(msg,1);
        success &= libusb_release_interface(m_handle, 0) == LIBUSB_SUCCESS;
        libusb_close(m_handle);
        m_handle = NULL;
    }
    m_hwCurrent = HW_NONE;

    return success;
}

// ----------------------------------------------------------------------------
// HardwareUC - private functions
// ----------------------------------------------------------------------------

int HardwareUC::readString(unsigned char *msg, int size, bool noerror) const {
    if (m_handle == NULL)
        return -1;

    int nBytes, retcode;
    if (m_modeReadEndpoint == LIBUSB_TRANSFER_TYPE_INTERRUPT)
        retcode = libusb_interrupt_transfer(m_handle, READ_ENDPOINT, msg, size,
                                            &nBytes, USB_OPERATION_TIMEOUT);
    else
        retcode = libusb_bulk_transfer(m_handle, READ_ENDPOINT, msg, size,
                                       &nBytes, USB_OPERATION_TIMEOUT);

    if (!noerror && retcode != LIBUSB_SUCCESS) {
// FIXME: in bootloader mode and in Windows, it sometimes gives a timeout
// immediately after a reconnect.
#ifndef __WIN32__
// wxLogError(_("USB error while reading: %s"),
// libusb_strerror((libusb_error)retcode));
#endif
        return -1;
    }
    return nBytes;
}

int HardwareUC::writeString(const unsigned char *msg, int size,
                          bool noerror) const {
    if (m_handle == NULL)
        return -1;

    // printf( "writeString: 0x%02X 0x%02X (%d)\n", msg[0], msg[1], msg[1] );

    int nBytes, retcode;
    if (m_modeWriteEndpoint == LIBUSB_TRANSFER_TYPE_INTERRUPT)
        retcode = libusb_interrupt_transfer(m_handle, WRITE_ENDPOINT,
                                            (unsigned char *)msg, size, &nBytes,
                                            USB_OPERATION_TIMEOUT);
    else
        retcode =
            libusb_bulk_transfer(m_handle, WRITE_ENDPOINT, (unsigned char *)msg,
                                 size, &nBytes, USB_OPERATION_TIMEOUT);

    if (!noerror && (retcode != LIBUSB_SUCCESS || nBytes < size)) {
        // wxLogError(_("USB error while writing to device: %d bytes, errCode:
        // %d; %s"), size, nBytes,
        //           libusb_strerror((libusb_error)retcode));
        return -1;
    }

    return nBytes;
}

void HardwareUC::tryToDetachDriver() {
// try to detach an already existing driver... (linux only)
#if defined(LIBUSB_HAS_GET_DRIVER_NP) && LIBUSB_HAS_GET_DRIVER_NP
    //  log(Log::DebugLevel::Extra, "find if there is already an installed
    //  driver");
    char dname[256] = "";
    if (usb_get_driver_np(m_handle, 0, dname, 255) < 0)
        return;
//  log(Log::DebugLevel::Normal, QString("  a driver \"%1\" is already
//  installed...").arg(dname));
#if defined(LIBUSB_HAS_DETACH_KERNEL_DRIVER_NP) &&                             \
    LIBUSB_HAS_DETACH_KERNEL_DRIVER_NP
    usb_detach_kernel_driver_np(m_handle, 0);
    // log(Log::DebugLevel::Normal, "  try to detach it...");
    if (usb_get_driver_np(m_handle, 0, dname, 255) < 0)
        return;
// log(Log::DebugLevel::Normal, "  failed to detach it");
#endif
#endif
}
