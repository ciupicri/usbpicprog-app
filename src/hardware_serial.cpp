/***************************************************************************
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

// NOTE: to avoid lots of warnings with MSVC 2008 about deprecated CRT functions
//       it's important to include wx/defs.h before STL headers
#include <wx/defs.h>

// IMPORTANT: first include wxWidgets headers (indirectly)
#include <wx/log.h>

/* FIXME: The following will break windows */
#include <termios.h>
#include <unistd.h>
#include <errno.h>

#include "hardware_serial.h"
#include "uppmainwindow.h"

#ifdef __WXMSW__
#include <wx/msw/msvcrt.h> // useful to catch memory leaks when compiling under MSVC
#endif

using namespace std;

#ifdef DEBUG
#define dprintf(...)    printf(__VA_ARGS__)
#else
#define dprintf(...)
#endif

// ----------------------------------------------------------------------------
// HardwareSerial
// ----------------------------------------------------------------------------
HardwareSerial::HardwareSerial() {
    // init all internal variables
    m_handle = -1;
    m_deviceName = DEFAULT_SERIAL_DEVICE;
    /* tested for B115.2Kbps to B1Mbps */
    m_baudex = B115200;
    m_hwCurrent = HW_UPP;
    m_protocol = PROT_UPP0;
}

HardwareSerial::~HardwareSerial() {
    disconnect();
}

void HardwareSerial::setConnParams(const char *deviceName)
{
    m_deviceName = deviceName;
}


void HardwareSerial::resetTarget(uint8_t mode)
{
#define CMD_RESET_TARGET	0xF1
   uint8_t reset_msg[] = {
       CMD_RESET_TARGET,
       mode
   };
   writeString(reset_msg, sizeof(reset_msg), true);
   usleep(1000*1000);
}

bool HardwareSerial::targetDeviceGood() {
#define CMD_SYNC	        0xF0
#define RESET_STATE_ICSP    0x1

    bool sync = false;
    uint8_t sync_msg[] = {CMD_SYNC, 0xAA, 0xAA};
    uint16_t reply_sync, expected_reply = 0xd555; // arithmetic shift
    int tries = 9;

    resetTarget(RESET_STATE_ICSP);
    // check to see if device is alive
    while(!sync && tries--) {
        // give a command and check against a known reply
        writeString(sync_msg, sizeof(sync_msg), true);
        readString((uint8_t *)&reply_sync, 2, true);
        if(reply_sync == expected_reply)
            return true;

        // wait for one tenth of a second before re-trying
        usleep(1000*100);

        // with enough tries and the board didnt come up, do a target reset
        if(tries % 5 == 0) {
            resetTarget(RESET_STATE_ICSP);
        }
    }
    return false;
}


bool HardwareSerial::connect(UppMainWindow *CB) {
    int retcode;
    struct termios rawtio;
    int nBytes;

    m_pCallBack = CB;

    m_handle = open(m_deviceName, O_RDWR|O_NOCTTY);
    if (m_handle < 0)
        goto open_err;

    // backup current settings
    tcgetattr(m_handle, &m_oldtio);

    // set raw mode and extended speed, 1Mbps
    rawtio.c_cflag = CBAUDEX;
    cfsetispeed(&rawtio, m_baudex);
    cfsetospeed(&rawtio, m_baudex);
    cfmakeraw(&rawtio);

    // wait for 5secs before timeout
    rawtio.c_cc[VMIN] = 0;
    rawtio.c_cc[VTIME] = 30;

    tcflush(m_handle, TCIFLUSH);
    if(tcsetattr(m_handle, TCSANOW, &rawtio) < 0)
        goto config_err;

    if(!targetDeviceGood())
        goto target_err;

    // everything completed successfully:
    unsigned char msg[64];

    msg[0] = CMD_GET_PROTOCOL_VERSION;
    writeString(msg, 1, true); // ignore errors (if we are disconnected then
                               // we will figure it out next time)
    nBytes = readString(msg, 1, true);
    if (nBytes > 0)
        m_protocol = (PROTOCOL)(PROT_UPP0 + msg[0]);
    else
        m_protocol = PROT_UPP0;

    return true;

target_err:
    tcsetattr(m_handle, TCSANOW, &m_oldtio);
    errno = -EIO;

config_err:
    close(m_handle);

open_err:
    m_handle = -1;
    return false;
}

bool HardwareSerial::disconnect() {

    if (m_handle) {
        /* restore settings */
        tcsetattr(m_handle, TCSANOW, &m_oldtio);
        close(m_handle);
    }
    return true;
}

void dumpData(const char *msg, const uint8_t *data, int size)
{
    int i;
    uint32_t off = 0;
    dprintf("%s:[%d]", msg, size);
    for(i=0; i<size; i++) {
	    if(i % 16 == 0) {
		    dprintf("\n%04x: ", off + i);
	    }
        dprintf("%02x ", data[i]);

    }
    dprintf("\n");
}

/* try hard to read size bytes */
int HardwareSerial::readBytes(uint8_t *msg, int size) const {
        int readsize = 0;
        int ret;
        while(readsize < size)
        {
            ret = ::read(m_handle, msg + readsize, size - readsize);
            if(ret <= 0)
                return ret;
            readsize += ret;
        }
        return readsize;
}

int HardwareSerial::readString(unsigned char *msg, int size, bool noerror) const {
    int ret = -EBADF;
    uint8_t data[MAX_RESP_SIZE];
    struct response_header r;

    if (connected()) {
        /* read the response magic and size */
        ret = readBytes((uint8_t *)&r, sizeof(r));
        if(ret < 0)
            goto io_err;

        if(ret < sizeof(r)) {
            ret = -EIO;
            goto timeout_err;
        }
        // check for magic
        if(r.magic != RESP_MAGIC)
            goto msg_err;

        // some sanity check on size
        if(r.size > MAX_RESP_SIZE)
            goto size_err;

        dumpData("readmagic", (uint8_t*)&r, ret);

        // read actual response packet now
        // FIXME: if received less than expected, reset the controller
        ret = readBytes(data, r.size);
        memcpy(msg, data, min(r.size, size));
        dumpData(__func__, msg, ret);
    }
    return ret;

io_err:
    dprintf("read error:%s\n", strerror(errno));
    ret = errno;
    // fall through

timeout_err:
    return ret;

msg_err:
size_err:
    dumpData("readerr:", (uint8_t *)&r, ret);
    return -EIO;

}

int HardwareSerial::writeString(const unsigned char *msg, int size,
                          bool noerror) const {

    int ret = -EBADF;
    if (connected()) {
        ret = ::write(m_handle, msg, size);
        dumpData("writecmd:", msg, ret);
    }
    /* send error only when it is allowed */
    return ret;
}

